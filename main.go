package main

// generate @font-face CSS fragment for font

import (
	"flag"
	"fmt"
	"log"
	"os"
)

import (
	"gitlab.com/font8/opentype"
)

func main() {
	flag.Parse()

	// Keep track of which Font Family Names have
	// already been used.
	// The value-side of the map is the highest ordinal
	// used after that Family Name.
	usedNames := map[string]int{}

	for _, arg := range flag.Args() {
		Face(arg, usedNames)
	}
}

func Face(name string, used map[string]int) {
	r, err := os.Open(name)
	if err != nil {
		log.Print(err)
		return
	}

	font, err := opentype.OpenFont(r)
	if err != nil {
		log.Print(err)
		return
	}

	table, ok := font.Table["name"]
	if !ok {
		return
	}
	nametable := opentype.NewName(table)
	orig_familyname := nametable.FullName()
	familyname := orig_familyname

	o := 0
	for {
		oo, seen := used[familyname]
		if !seen {
			break
		}
		o = oo + 1
		orig_familyname = familyname
		familyname = fmt.Sprintf("%s %d", familyname, o)
	}

	used[orig_familyname] = o

	fmt.Println("@font-face {")
	fmt.Println("font-family: '" + familyname + "';")
	fmt.Println("src: url(" + name + ");")
	fmt.Println("}")
}
